const mostrarDatos = (pelicula) => {
    const contenedor = document.getElementById('respuesta-6');
    contenedor.innerHTML = '';
  
    const crearElemento = (clase, contenido) => {
      const elemento = document.createElement('div');
      elemento.className = clase;
      elemento.innerHTML = contenido;
      return elemento;
    }
  
    const elementoInfo = crearElemento('info', `
      <p><b>Nombre:</b> ${pelicula.Title}</p>
      <p><b>Año de Realizacion:</b> ${pelicula.Year}</p>
    `);
  
    const elementoImagen = crearElemento('imagen', `
      <img src="${pelicula.Poster}" alt="Imagen de ${pelicula.Title}">
    `);
  
    const elementoReseña = crearElemento('reseña', `
      <span><p><b>Reseña:</b></p> <span>${pelicula.Plot}</span></span>
    `);
  
    const elementoActores = crearElemento('actores', `
      <p><b>Actores principales:</b> ${pelicula.Actors}</p>
    `);
  
    contenedor.appendChild(elementoInfo);
    contenedor.appendChild(elementoImagen);
    contenedor.appendChild(elementoReseña);
    contenedor.appendChild(elementoActores);
  }

document.getElementById("formularioBusqueda").addEventListener('submit', function(event) {
    event.preventDefault();
    let nombrePelicula = document.getElementById('inputName').value.trim();

    if (nombrePelicula) {
        buscarPeliculaExacta(nombrePelicula);
    } else {
        alert("Por favor, ingrese un nombre de pelicula completo para buscar.");
    }
});

const buscarPeliculaExacta = async (nombrePelicula) => {
    const url = `https://www.omdbapi.com/?t=${nombrePelicula}&plot=full&apikey=30063268`;

    try {
        const response = await fetch(url);
        const pelicula = await response.json();
        if (pelicula && pelicula.Title && pelicula.Title.toLowerCase() === nombrePelicula.toLowerCase()) {
            mostrarDatos(pelicula); // Mostrar solo la película que coincide exactamente
        } else {
            alert('No se encontró una pelicula con ese nombre exacto.');
        }
    } catch (err) {
        console.log("Surgió un error: " + err);
    }
};

document.getElementById("btnLimpiar").addEventListener('click', function(event) {
    event.preventDefault();
    document.getElementById('respuesta-6').innerHTML = "";
    document.getElementById('inputName').value = "";
});
